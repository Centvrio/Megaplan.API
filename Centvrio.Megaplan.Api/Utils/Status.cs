﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Utils
{
    public struct Status
    {
        [JsonProperty("code")]
        public string Code { get; private set; }

        [JsonProperty("message")]
        public object Message { get; private set; }
    }

    internal struct MessageInternal
    {
        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("field")]
        public string Field { get; set; }
    }
}