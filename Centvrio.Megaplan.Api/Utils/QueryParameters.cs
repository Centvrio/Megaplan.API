﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Centvrio.Megaplan.Api.Utils
{
    public class QueryParameters
    {
        private readonly IList<KeyValuePair<string, string>> storage = new List<KeyValuePair<string, string>>();

        public int Count => storage.Count;

        internal IList<KeyValuePair<string, string>> Storage => storage;

        public static QueryParameters Create() => new QueryParameters();

        public static QueryParameters CreateAndAdd<T>(string name, T value) => new QueryParameters().Add(name, value);

        public object this[string name]
        {
            set => Add(name, value);
        }

        public QueryParameters Add<T>(string name, T value)
        {
            storage.Add(new KeyValuePair<string, string>(name, value.ToString()));
            return this;
        }

        public QueryParameters AddByContidion<T>(Func<bool> predicate, string name, T value)
        {
            bool predicateResult = predicate?.Invoke() ?? false;
            if (predicateResult)
            {
                return Add(name, value);
            }
            return this;
        }

        public QueryParameters AddByArrayContidion<T>(Func<bool> predicate, string name, params T[] value)
        {
            bool predicateResult = predicate?.Invoke() ?? false;
            if (predicateResult)
            {
                return AddArray(name, value);
            }
            return this;
        }

        public QueryParameters AddArray<T>(string name, params T[] values)
        {
            if (values != null)
            {
                foreach (T value in values)
                {
                    Add(name + "[]", value);
                }
            }
            return this;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            int counter = 0;
            foreach (KeyValuePair<string, string> pair in this)
            {
                if (counter > 0)
                {
                    builder.Append("&");
                }
                builder.AppendFormat("{0}={1}", pair.Key, Uri.EscapeDataString(pair.Value));
                counter++;
            }
            return builder.ToString();
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => storage.GetEnumerator();
    }
}
