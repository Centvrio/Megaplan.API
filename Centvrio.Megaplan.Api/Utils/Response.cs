﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Utils
{
    public class Response<T>
    {
        [JsonProperty("status")]
        public Status Status { get; internal set; }

        [JsonProperty("data")]
        public virtual T Data { get; internal set; }

        [JsonIgnore]
        public bool IsSuccess => Status.Code == "ok";
    }
}
