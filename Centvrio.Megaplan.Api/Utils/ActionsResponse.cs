﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Utils
{
    public class ActionsResponse<T> : Response<T>
    {
        [JsonProperty("actions")]
        public override T Data { get; internal set; }
    }
}
