﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Utils
{
    public class DeadlineChangeResponse : Response<int>
    {
        [JsonProperty("createdDeadlineChangeId")]
        public override int Data { get; internal set; }
    }
}
