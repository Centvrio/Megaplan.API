﻿using System;

namespace Centvrio.Megaplan.Api.Exceptions
{
    public class CoreException : Exception
    {
        public CoreException(string message) : base(message)
        {
        }
    }
}
