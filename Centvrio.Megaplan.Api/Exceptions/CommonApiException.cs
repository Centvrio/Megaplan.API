﻿using System;
using System.Net;

namespace Centvrio.Megaplan.Api.Exceptions
{
    public class CommonApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public CommonApiException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
