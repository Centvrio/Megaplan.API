﻿namespace Centvrio.Megaplan.Api.Injection
{
    public interface IAuthorizationPair
    {
        string AccessId { get; set; }

        string SecretKey { get; set; }
    }
}
