﻿namespace Centvrio.Megaplan.Api.Injection
{
    public interface ISession : IAuthorizationPair
    {
        string Host { get; set; }
        bool IsAuthorized { get; set; }
    }
}
