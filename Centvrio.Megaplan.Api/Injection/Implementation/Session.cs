﻿namespace Centvrio.Megaplan.Api.Injection.Implementation
{
    public class Session : ISession
    {
        public string Host { get; set; }
        public string AccessId { get; set; }
        public string SecretKey { get; set; }
        public bool IsAuthorized { get; set; }
    }
}
