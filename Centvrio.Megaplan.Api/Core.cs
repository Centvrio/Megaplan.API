﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Exceptions;
using Centvrio.Megaplan.Api.Injection;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Centvrio.Megaplan.Api
{
    public class Core
    {
        private delegate T CommonApiConstructorDelegate<T>(ISession session) where T : CommonApi;

         private ISession session;

        private CommonApi lastUsed;

        private readonly ObjectActivator activator;

        [Obsolete]
        Core()
        {
            activator = new ObjectActivator();
        }

        Core(ISession session)
        {
            activator = new ObjectActivator();
            this.session = session;
        }

        public static Core Create<T>(T session) where T : ISession
        {
            if (session == null)
            {
                throw new ArgumentNullException(nameof(session));
            }

            if (string.IsNullOrEmpty(session.Host))
            {
                throw new CoreException("Host can't be empty.");
            }

            return new Core(session);
        }

        public T Take<T>() where T : CommonApi
        {
            if (lastUsed is T result)
            {
                return result;
            }
            lastUsed = activator.CreateInstance<T>(session);
            return (T)lastUsed;
        }

        public ISession Session { get => session; }

        public bool IsCreated => session != null;
    }
}
