﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Injection;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Centvrio.Megaplan.Api.Tests")]
namespace Centvrio.Megaplan.Api
{
    public class ObjectActivator
    {
        private readonly IDictionary<Type, Delegate> cache = new Dictionary<Type, Delegate>();

        private delegate T CommonApiConstructor<T>(ISession session) where T : CommonApi;

        internal int CacheSize => cache.Count;

        private CommonApiConstructor<T> CreateActivator<T>() where T : CommonApi
        {
            ConstructorInfo constructor = typeof(T).GetConstructor(new[] { typeof(ISession) });
            ParameterExpression parameter = Expression.Parameter(typeof(ISession));
            NewExpression body = Expression.New(constructor, parameter);
            var constructorDelegate = Expression.Lambda<CommonApiConstructor<T>>(body, parameter);
            CommonApiConstructor<T> invoked = constructorDelegate.Compile();
            cache[typeof(T)] = invoked;
            return invoked;
        }

        public T CreateInstance<T>(ISession session) where T : CommonApi
        {
            if (cache.ContainsKey(typeof(T)))
            {
                return (cache[typeof(T)] as CommonApiConstructor<T>).Invoke(session);
            }
            return CreateActivator<T>().Invoke(session);
        }
    }
}
