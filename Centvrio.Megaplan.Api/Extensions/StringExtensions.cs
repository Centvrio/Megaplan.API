﻿using System.Security.Cryptography;
using System.Text;

namespace Centvrio.Megaplan.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToMd5(this string source)
        {
            using (var md5 = MD5.Create())
            {
                byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(source));
                return HashToString(data);
            }
        }

        public static string ToHmacSha1(this string source, string key)
        {
            using (var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(key)))
            {
                byte[] data = hmac.ComputeHash(Encoding.UTF8.GetBytes(source));
                return HashToString(data);
            }
        }

        private static string HashToString(byte[] hash)
        {
            var builder = new StringBuilder();
            int length = hash.Length;
            for (int i = 0; i < length; i++)
            {
                builder.Append(hash[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
