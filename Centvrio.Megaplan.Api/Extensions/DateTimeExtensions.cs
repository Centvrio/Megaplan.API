﻿using System;
using System.Globalization;

namespace Centvrio.Megaplan.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToRfc2822(this DateTime dateTime)
        {
            TimeSpan offset = TimeZoneInfo.Local.GetUtcOffset(dateTime);
            string prepared = dateTime.ToString("ddd, dd MMM yyyy HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
            return $"{prepared} {offset.Hours:+0#;-0#}{offset.Minutes:D2}";
        }

        public static DateTimeOffset FromRfc2822(this string s)
        {
            int length = s.Length;
            string dateString = s.Substring(0, length - 6);
            string hoursString = s.Substring(length - 5, 3);
            string minutesString = s.Substring(length - 2, 2);
            DateTimeOffset result = DateTimeOffset.UtcNow;
            if (DateTime.TryParse(dateString, out var date))
            {
                if (int.TryParse(hoursString, out int offsetHours) && int.TryParse(minutesString, out int offsetMinutes))
                {
                    TimeSpan offset = TimeSpan.FromHours(offsetHours)
                        .Add(TimeSpan.FromMinutes(offsetMinutes));
                    result = new DateTimeOffset(date, offset);
                }
            }
            return result;
        }
    }
}
