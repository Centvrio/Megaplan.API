﻿using Centvrio.Megaplan.Api.Common;
using System.Linq;

namespace Centvrio.Megaplan.Api.Extensions
{
    public static class EnumExtensions
    {
        public static string ToStringAction(this ProjectActions source) => PascalCaseToUnderscored(source.ToString(), "act_");

        public static string ToStringAction(this TaskActions source) => PascalCaseToUnderscored(source.ToString(), "act_");

        public static string ToStringAction(this EmployeeActions source) => PascalCaseToUnderscored(source.ToString(), "act_");

        private static string PascalCaseToUnderscored(string str, string prefix)
        {
            string result = string
                .Concat(str.Select((chr, index) => index > 0 && char.IsUpper(chr) ? $"_{chr}" : chr.ToString()))
                .ToLower();
            return string.IsNullOrEmpty(prefix) ? result : prefix + result;
        }
    }
}
