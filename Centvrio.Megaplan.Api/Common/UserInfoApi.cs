﻿using Centvrio.Megaplan.Api.Injection;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class UserInfoApi : CommonApi
    {
        public UserInfoApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>()
        {
            T result = await Network.Get<T>("/BumsCommonApiV01/UserInfo/id.api");
            return result;
        }
    }
}
