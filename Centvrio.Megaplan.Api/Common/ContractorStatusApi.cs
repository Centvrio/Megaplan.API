﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class ContractorStatusApi : CommonApi
    {
        public ContractorStatusApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            StatusResult<T> result = await Network.Get<StatusResult<T>>("/BumsCrmApiV01/ContractorStatus/card.api", query);
            return result.Result;
        }

        public async Task<int> Create(string name)
        {
            var data = QueryParameters
                .CreateAndAdd("Model[Name]", name);
            StatusResult<EntityId> result = await Network.Post<StatusResult<EntityId>>("/BumsCrmApiV01/ContractorStatus/save.api", data);
            return result.Result;
        }

        public async Task<int> Edit(int id, string name)
        {
            QueryParameters data = QueryParameters
                .CreateAndAdd("Id", id)
                .Add("Model[Name]", name);
            StatusResult<EntityId> result = await Network.Post<StatusResult<EntityId>>("/BumsCrmApiV01/ContractorStatus/save.api", data);
            return result.Result;
        }

        public async Task Delete(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCrmApiV01/ContractorStatus/delete.api", query);
        }
    }
}
