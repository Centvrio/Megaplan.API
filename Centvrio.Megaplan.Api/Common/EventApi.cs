﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class EventApi : CommonApi
    {
        public EventApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            EventResult<T> result = await Network.Get<EventResult<T>>("/BumsTimeApiV01/Event/card.api", query);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            IList<T> result = await Network.Get<IList<T>>("/BumsTimeApiV01/Event/list.api", filter);
            return result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> Categories<T>(QueryParameters filter)
        {
            EventCategoryMultipleResult<T> result = await Network.Get<EventCategoryMultipleResult<T>>("/BumsTimeApiV01/Event/categories.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> Categories<T>()
        {
            return await Categories<T>(null);
        }

        public async Task<IList<T>> Places<T>()
        {
            EventPlacesMultipleResult<T> result = await Network.Get<EventPlacesMultipleResult<T>>("/BumsTimeApiV01/Event/places.api");
            return result.Result;
        }

        public async Task<IDictionary<string, T>> Create<T>(QueryParameters data)
        {
            IDictionary<string, T> result = await Network.Post<IDictionary<string, T>>("/BumsTimeApiV01/Event/create.api", data);
            return result;
        }

        public async Task<IDictionary<string, T>> Edit<T>(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            IDictionary<string, T> result = await Network.Post<IDictionary<string, T>>("/BumsTimeApiV01/Event/update.api", data);
            return result;
        }

        public async Task Delete(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsTimeApiV01/Event/delete.api", query);
        }

        public async Task Finish(int id, bool isFinish)
        {
            await Finish(id, isFinish, null, false);
        }

        public async Task Finish(int id, bool isFinish, string result)
        {
            await Finish(id, isFinish, result, false);
        }

        public async Task Finish(int id, bool isFinish, string result, bool canSendLetter)
        {
            QueryParameters data = PreparedFinish(id, isFinish, result, canSendLetter);
            await Network.Post<EmptyResult>("/BumsTimeApiV01/Event/finish.api", data);
        }

        private QueryParameters PreparedFinish(int id, bool isFinish, string result, bool canSendLetter = false)
        {
            QueryParameters data = QueryParameters.CreateAndAdd("Id", id)
                .Add("Finish", isFinish)
                .Add("SendLetter", canSendLetter);
            if (!string.IsNullOrEmpty(result))
            {
                data.Add("Result", result);
            }
            return data;
        }
    }
}
