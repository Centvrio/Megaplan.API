﻿using Centvrio.Megaplan.Api.Extensions;
using Newtonsoft.Json;
using System;

namespace Centvrio.Megaplan.Api.Common.Converters
{
    public class DateRfc2822Converter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(DateTimeOffset);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            object value = reader.Value;
            if (value is string s && !string.IsNullOrEmpty(s))
            {
                return s.FromRfc2822();
            }
            return DateTimeOffset.UtcNow;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTimeOffset dt)
            {
                writer.WriteValue(dt.DateTime.ToRfc2822());
            }
        }
    }
}
