﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Extensions;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class UserApi : CommonApi
    {
        public UserApi(ISession session) : base(session)
        {
        }

        public async Task<AuthorizationResult> Login(string login, string password)
        {
            string passwordHash = password.ToMd5();
            AuthorizationResult result = await Network.Get<AuthorizationResult>("/BumsCommonApiV01/User/authorize.api", new QueryParameters
            {
                ["Login"] = login,
                ["Password"] = passwordHash
            });
            Session.AccessId = result.AccessId;
            Session.SecretKey = result.SecretKey;
            Session.IsAuthorized = true;
            return result;
        }

        public async Task<AuthorizationResult> Login(string oneTimeKey)
        {
            AuthorizationResult result = await Network.Get<AuthorizationResult>("/BumsCommonApiV01/User/authorize.api", QueryParameters.Create().Add("OneTimeKey", oneTimeKey));
            Session.AccessId = result.AccessId;
            Session.SecretKey = result.SecretKey;
            Session.IsAuthorized = true;
            return result;
        }

        public async Task<AuthorizationResult> GetOneTimeKey(string login, string password)
        {
            string passwordHash = password.ToMd5();
            AuthorizationResult result = await Network.Get<AuthorizationResult>("/BumsCommonApiV01/User/authorize.api", new QueryParameters()
            {
                ["Login"] = login,
                ["Password"] = passwordHash
            });
            return result;
        }
    }
}
