﻿using Centvrio.Megaplan.Api.Common.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct EntityId
    {
        [JsonProperty("Id")]
        public int? Id { get; set; }

        public static implicit operator int(EntityId entityId)
        {
            return entityId.Id ?? 0;
        }
    }

    internal struct EntityField<T>
    {
        [JsonProperty("Fields")]
        public IList<T> Fields { get; set; }
    }

    internal struct EntityDateTime
    {
        [JsonProperty("datetime")]
        [JsonConverter(typeof(DateRfc2822Converter))]
        public DateTimeOffset DateTime { get; set; }
    }
}
