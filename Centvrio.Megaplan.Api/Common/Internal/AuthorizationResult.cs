﻿using Centvrio.Megaplan.Api.Injection;
using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    public class AuthorizationResult : IAuthorizationPair
    {
        [JsonProperty("AccessId")]
        public string AccessId { get; set; }

        [JsonProperty("SecretKey")]
        public string SecretKey { get; set; }

        [JsonProperty("UserId")]
        public int? UserId { get; set; }

        [JsonProperty("EmployeeId")]
        public int? EmployeeId { get; set; }
    }
}
