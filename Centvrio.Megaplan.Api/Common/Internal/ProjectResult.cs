﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct ProjectResult<T>
    {
        [JsonProperty("project")]
        public T Result { get; internal set; }
    }

    internal struct ProjectMultipleResult<T>
    {
        [JsonProperty("projects")]
        public IList<T> Result { get; internal set; }
    }
}
