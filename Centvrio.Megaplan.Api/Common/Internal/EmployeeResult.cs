﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct EmployeeResult<T>
    {
        [JsonProperty("employee")]
        public T Result { get; internal set; }
    }

    internal struct EmployeeMultipleResult<T>
    {
        [JsonProperty("employees")]
        public IList<T> Result { get; internal set; }
    }

    internal struct EmployeeDepartmentsResult<T>
    {
        [JsonProperty("departments")]
        public IList<T> Result { get; internal set; }
    }

    internal struct EmployeeOnlineResult<T>
    {
        [JsonProperty("Ids")]
        public IList<T> Result { get; internal set; }
    }
}
