﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct TaskResult<T>
    {
        [JsonProperty("task")]
        public T Result { get; internal set; }
    }

    internal struct TaskMiltipleResult<T>
    {
        [JsonProperty("tasks")]
        public IList<T> Result { get; internal set; }
    }
}
