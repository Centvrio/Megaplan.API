﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct DealResult<T>
    {
        [JsonProperty("deal")]
        public T Result { get; set; }
    }

    internal struct DealMultipleResult<T>
    {
        [JsonProperty("deals")]
        public IList<T> Result { get; set; }
    }

    internal struct ProgramMultipleResult<T>
    {
        [JsonProperty("programs")]
        public IList<T> Result { get; set; }
    }
}
