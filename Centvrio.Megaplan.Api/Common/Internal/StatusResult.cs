﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct StatusResult<T>
    {
        [JsonProperty("status")]
        public T Result { get; set; }
    }
}
