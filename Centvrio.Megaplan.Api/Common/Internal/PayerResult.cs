﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct PayerResult<T>
    {
        [JsonProperty("payer")]
        public T Result { get; set; }
    }

    internal struct PayerMultipleResult<T>
    {
        [JsonProperty("payers")]
        public IList<T> Result { get; set; }
    }
}
