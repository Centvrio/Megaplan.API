﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct EventResult<T>
    {
        [JsonProperty("event")]
        public T Result { get; set; }
    }

    internal struct EventCategoryMultipleResult<T>
    {
        [JsonProperty("categories")]
        public IList<T> Result { get; set; }
    }

    internal struct EventPlacesMultipleResult<T>
    {
        [JsonProperty("places")]
        public IList<T> Result { get; set; }
    }
}
