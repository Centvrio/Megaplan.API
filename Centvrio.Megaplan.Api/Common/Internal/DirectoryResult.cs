﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct CurrencyMultipleResult<T>
    {
        [JsonProperty("currencies")]
        public IList<T> Result { get; set; }
    }

    internal struct TaxMultipleResult<T>
    {
        [JsonProperty("taxes")]
        public IList<T> Result { get; set; }
    }

    internal struct PhoneTypeMultipleResult<T>
    {
        [JsonProperty("PhoneTypes")]
        public IList<T> Result { get; set; }
    }

    internal struct UnitMultipleResult<T>
    {
        [JsonProperty("units")]
        public IList<T> Result { get; set; }
    }
}
