﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct ContractorResult<T>
    {
        [JsonProperty("contractor")]
        public T Result { get; internal set; }
    }

    internal struct ContractorMultipleResult<T>
    {
        [JsonProperty("clients")]
        public IList<T> Result { get; internal set; }
    }
}
