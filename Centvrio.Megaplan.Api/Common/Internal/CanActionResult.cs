﻿using Newtonsoft.Json;

namespace Centvrio.Megaplan.Api.Common.Internal
{
    internal struct CanActionResult
    {
        [JsonProperty("CanCreate")]
        public bool? CanCreate { get; set; }

        [JsonProperty("CanDelegate")]
        public bool? CanDelegate { get; set; }
    }
}
