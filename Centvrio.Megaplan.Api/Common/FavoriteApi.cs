﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class FavoriteApi : CommonApi
    {
        public FavoriteApi(ISession session) : base(session)
        {
        }

        public async Task Add(int subjectId, string subjectType)
        {
            await Manage("/BumsCommonApiV01/Favorite/add.api", subjectId, subjectType);
        }

        public async Task Add(int subjectId, SubjectType subjectType)
        {
            await Add(subjectId, subjectType.ToString().ToLower());
        }

        public async Task Remove(int subjectId, string subjectType)
        {
            await Manage("/BumsCommonApiV01/Favorite/remove.api", subjectId, subjectType);
        }

        public async Task Remove(int subjectId, SubjectType subjectType)
        {
            await Remove(subjectId, subjectType.ToString().ToLower());
        }

        public async Task<T> GetAll<T>()
        {
            T result = await Network.Get<T>("/BumsCommonApiV01/Favorite/list.api");
            return result;
        }

        public async Task<T> GetAll<T>(DateTime timeUpdated)
        {
            var query = QueryParameters.CreateAndAdd("TimeUpdated", timeUpdated.ToString(CultureInfo.InvariantCulture));
            T result = await Network.Get<T>("/BumsCommonApiV01/Favorite/list.api", query);
            return result;
        }

        private async Task Manage(string url, int subjectId, string subjectType)
        {
            QueryParameters query = QueryParameters
                .CreateAndAdd("SubjectId", subjectId)
                .Add("SubjectType", subjectType);
            await Network.Get<EmptyResult>(url, query);
        }
    }
}
