﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class TodoListApi : CommonApi
    {
        public TodoListApi(ISession session) : base(session)
        {
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            IList<T> result = await Network.Get<IList<T>>("/BumsTimeApiV01/TodoList/list.api", filter);
            return result;
        }

        public async Task<int> Create(string name)
        {
            QueryParameters data = QueryParameters.Create().Add("Name", name);
            EntityId result = await Network.Post<EntityId>("/BumsTimeApiV01/TodoList/create.api", data);
            return result;
        }

        public async Task<int> Edit(int id, string name)
        {
            QueryParameters data = QueryParameters.Create()
                .Add("Id", id)
                .Add("Name", name);
            EntityId result = await Network.Post<EntityId>("/BumsTimeApiV01/TodoList/edit.api", data);
            return result;
        }

        public async Task<int> Delete(int id)
        {
            QueryParameters data = QueryParameters.Create()
                .Add("Id", id);
            EntityId result = await Network.Post<EntityId>("/BumsTimeApiV01/TodoList/delete.api", data);
            return result;
        }
    }
}
