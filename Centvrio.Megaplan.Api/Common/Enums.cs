﻿namespace Centvrio.Megaplan.Api.Common
{
    public enum ProjectActions
    {
        AcceptWork,
        RejectWork,
        Done,
        Pause,
        Resume,
        Cancel,
        Expire,
        Renew,
        Delete,
        Edit,
        Convert,
        CreateTask,
        CreateSubproject
    }

    public enum TaskActions
    {
        AcceptTask,
        RejectTask,
        AcceptWork,
        RejectWork,
        Done,
        Pause,
        Resume,
        Cancel,
        Expire,
        Renew,
        Delete,
        Edit,
        Convert,
        Subtask,
        Delegate,
        EditExecutors
    }

    public enum SubjectType
    {
        Project,
        Task,
        Comment,
        Event
    }

    public enum EmployeeActions
    {
        CanFire,
        Edit
    }

    public enum RelatedObjectType
    {
        Deal,
        Task,
        Project
    }

    public enum PayerType
    {
        Legal,
        Natural
    }
}
