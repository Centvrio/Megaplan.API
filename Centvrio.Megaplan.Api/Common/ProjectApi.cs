﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Extensions;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class ProjectApi : CommonApi
    {
        public ProjectApi(ISession session) : base(session)
        {
        }

        public async Task<T> AvailableActions<T>(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            T result = await Network.Get<T, ActionsResponse<T>>("/BumsProjectApiV01/Project/availableActions.api", query);
            return result;
        }

        public async Task<T> AvailableActionsForList<T>(params int[] ids)
        {
            var query = QueryParameters.Create();
            query.AddArray("Ids", ids);
            T result = await Network.Get<T, ActionsResponse<T>>("/BumsProjectApiV01/Project/availableActionsForList.api", query);
            return result;
        }

        public async Task Action(int id, string action)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("Action", action);
            await Network.Get<EmptyResult>("/BumsProjectApiV01/Project/action.api", query);
        }

        public async Task Action(int id, ProjectActions action)
        {
            await Action(id, action.ToStringAction());
        }

        public async Task<bool> CanCreate()
        {
            CanActionResult result = await Network.Get<CanActionResult>("/BumsProjectApiV01/Project/canCreate.api");
            return result.CanCreate == true;
        }

        public async Task<int> ConvertToTask(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            TaskResult<EntityId> result = await Network.Get<TaskResult<EntityId>>("/BumsProjectApiV01/Project/convert.api", query);
            return result.Result;
        }

        public async Task MarkAsFavorite(int id, bool AsFavorite)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("Value", AsFavorite ? 1 : 0);
            await Network.Get<EmptyResult>("/BumsProjectApiV01/Project/markAsFavorite.api", query);
        }

        public async Task<T> Get<T>(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            ProjectResult<T> result = await Network.Get<ProjectResult<T>>("/BumsProjectApiV01/Project/card.api", query);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            ProjectMultipleResult<T> result = await Network.Get<ProjectMultipleResult<T>>("/BumsProjectApiV01/Project/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<T> Create<T>(QueryParameters data)
        {
            ProjectResult<T> result =  await Network.Post<ProjectResult<T>>("/BumsProjectApiV01/Project/create.api", data);
            return result.Result;
        }

        public async Task Edit(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            await Network.Post<EmptyResult>("/BumsProjectApiV01/Project/edit.api", data);
        }
    }
}
