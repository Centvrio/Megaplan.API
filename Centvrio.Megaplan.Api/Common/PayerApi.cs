﻿using System;
using System.Collections.Generic;
using System.Text;
using Centvrio.Megaplan.Api.Injection;
using System.Threading.Tasks;
using Centvrio.Megaplan.Api.Utils;
using Centvrio.Megaplan.Api.Common.Internal;

namespace Centvrio.Megaplan.Api.Common
{
    public class PayerApi : CommonApi
    {
        public PayerApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            PayerResult<T> result = await Network.Get<PayerResult<T>>("/BumsCrmApiV01/Payer/card.api", query);
            return result.Result;
        }

        public async Task<T> Get<T>(int id, params string[] extraFields)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            if ((extraFields?.Length ?? 0) > 0)
            {
                query.AddArray("ExtraFields", extraFields);
            }
            T result = await Network.Get<T>("/BumsCrmApiV01/Payer/card.api", query);
            return result;
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            PayerMultipleResult<T> result = await Network.Get<PayerMultipleResult<T>>("/BumsCrmApiV01/Payer/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> GetFields<T>()
        {
            return await GetFields<T>(null, null);
        }

        public async Task<IList<T>> GetFields<T>(string payerType)
        {
            return await GetFields<T>(payerType, null);
        }

        public async Task<IList<T>> GetFields<T>(PayerType payerType)
        {
            return await GetFields<T>(payerType.ToString());
        }

        public async Task<IList<T>> GetFields<T>(PayerType payerType, string countryCode)
        {
            return await GetFields<T>(payerType.ToString(), countryCode);
        }

        public async Task<IList<T>> GetFields<T>(string payerType, string countryCode)
        {
            var query = QueryParameters.Create();
            query
                .AddByContidion(() => !string.IsNullOrEmpty(payerType), "PayerType", payerType)
                .AddByContidion(() => !string.IsNullOrEmpty(countryCode), "CountryCode", countryCode);
            EntityField<T> result = await Network.Get<EntityField<T>>("/BumsCrmApiV01/Payer/listFields.api", query);
            return result.Fields;
        }

        public async Task<int> Create(QueryParameters data)
        {
            PayerResult<EntityId> result = await Network.Post<PayerResult<EntityId>>("/BumsCrmApiV01/Payer/save.api", data);
            return result.Result;
        }

        public async Task<int> Edit(int payerId, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("PayerId", payerId);
            return await Create(data);
        }

        public async Task ChangeContractor(int payerId, int oldContaractorId, int newContractorId)
        {
            QueryParameters data = QueryParameters.CreateAndAdd("PayerId", payerId)
                .Add("ContractorId", oldContaractorId)
                .Add("NewContractorId", newContractorId);
            await Network.Post<EmptyResult>("/BumsCrmApiV01/Payer/changeContractor.api", data);
        }
    }
}
