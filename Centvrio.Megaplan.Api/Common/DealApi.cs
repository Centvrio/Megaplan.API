﻿using System;
using System.Collections.Generic;
using System.Text;
using Centvrio.Megaplan.Api.Injection;
using System.Threading.Tasks;
using Centvrio.Megaplan.Api.Utils;
using Centvrio.Megaplan.Api.Common.Internal;

namespace Centvrio.Megaplan.Api.Common
{
    public class DealApi : CommonApi
    {
        public DealApi(ISession session) : base(session)
        {
        }

        public async Task<IList<T>> GetFields<T>(int programId)
        {
            var query = QueryParameters.CreateAndAdd("ProgramId", programId);
            EntityField<T> result = await Network.Get<EntityField<T>>("/BumsTradeApiV01/Deal/listFields.api", query);
            return result.Fields;
        }

        public async Task<T> Get<T>(int id)
        {
            return await Get<T>(id, null);
        }

        public async Task<T> Get<T>(int id, QueryParameters parameters)
        {
            QueryParameters query = parameters ?? QueryParameters.Create();
            query.Add("Id", id);
            DealResult<T> result = await Network.Get<DealResult<T>>("/BumsTradeApiV01/Deal/card.api", query);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            DealMultipleResult<T> result = await Network.Get<DealMultipleResult<T>>("/BumsTradeApiV01/Deal/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> GetProgramSchemas<T>()
        {
            ProgramMultipleResult<T> result = await Network.Get<ProgramMultipleResult<T>>("/BumsTradeApiV01/Program/list.api");
            return result.Result;
        }

        public async Task<IList<T>> GetProgramSchemas<T>(int limit)
        {
            return await GetProgramSchemas<T>(limit, 0);
        }

        public async Task<IList<T>> GetProgramSchemas<T>(int limit, int offset)
        {
            var query = QueryParameters.Create();
            query.AddByContidion(() => limit > 0, "Limit", limit);
            query.AddByContidion(() => offset > 0, "Offset", offset);
            ProgramMultipleResult<T> result = await Network.Get<ProgramMultipleResult<T>>("/BumsTradeApiV01/Program/list.api", query);
            return result.Result;
        }

        public async Task<T> Create<T>(QueryParameters data)
        {
            DealResult<T> result = await Network.Post<DealResult<T>>("/BumsTradeApiV01/Deal/save.api", data);
            return result.Result;
        }

        public async Task<T> Edit<T>(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            return await Create<T>(data);
        }

        public async Task<T> SaveRelation<T>(int id, int relatedObjectId, string relatedObjectType)
        {
            return await ManageRelation<T>("/BumsTradeApiV01/Deal/saveRelation.api", id, relatedObjectId, relatedObjectType);
        }

        public async Task<T> RemoveRelation<T>(int id, int relatedObjectId, string relatedObjectType)
        {
            return await ManageRelation<T>("/BumsTradeApiV01/Deal/removeRelation.api", id, relatedObjectId, relatedObjectType);
        }

        public async Task<T> SaveRelation<T>(int id, int relatedObjectId, RelatedObjectType relatedObjectType)
        {
            return await SaveRelation<T>(id, relatedObjectId, relatedObjectType.ToString().ToLower());
        }

        public async Task<T> RemoveRelation<T>(int id, int relatedObjectId, RelatedObjectType relatedObjectType)
        {
            return await RemoveRelation<T>(id, relatedObjectId, relatedObjectType.ToString().ToLower());
        }

        private async Task<T> ManageRelation<T>(string url, int id, int relatedObjectId, string relatedObjectType)
        {
            QueryParameters query = QueryParameters.CreateAndAdd("Id", id)
                .Add("RelatedObjectId", relatedObjectId)
                .Add("RelatedObjectType", relatedObjectType);
            DealResult<T> result = await Network.Get<DealResult<T>>(url, query);
            return result.Result;
        }
    }
}
