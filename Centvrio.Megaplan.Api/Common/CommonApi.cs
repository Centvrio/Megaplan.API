﻿using Centvrio.Megaplan.Api.Injection;

namespace Centvrio.Megaplan.Api.Common
{
    public abstract class CommonApi
    {
        protected ISession Session { get; set; }
        protected Network Network { get; }

        public CommonApi(ISession session)
        {
            Session = session;
            Network = new Network(Session);
        }
    }
}
