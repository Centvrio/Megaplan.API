﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class SystemApi : CommonApi
    {
        public SystemApi(ISession session) : base(session)
        {
        }

        public async Task Feedback(string message)
        {
            var query = QueryParameters.CreateAndAdd("Message", message);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/System/feedback.api", query);
        }

        public async Task<DateTimeOffset> DateTime()
        {
            EntityDateTime result = await Network.Get<EntityDateTime>("/BumsCommonApiV01/System/datetime.api");
            return result.DateTime;
        }
    }
}
