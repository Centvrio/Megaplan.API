﻿using Centvrio.Megaplan.Api.Injection;
using System.Threading.Tasks;
using Centvrio.Megaplan.Api.Utils;
using Centvrio.Megaplan.Api.Common.Internal;
using System.Collections.Generic;

namespace Centvrio.Megaplan.Api.Common
{
    public class ContractorApi : CommonApi
    {
        public ContractorApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>(int id, params string[] requestedFields)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .AddArray("RequestedFields", requestedFields);
            ContractorResult<T> result = await Network.Get<ContractorResult<T>>("/BumsCrmApiV01/Contractor/card.api", query);
            return result.Result;
        }

        public async Task<T> Get<T>(int id)
        {
            return await Get<T>(id, null);
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            ContractorMultipleResult<T> result = await Network.Get<ContractorMultipleResult<T>>("/BumsCrmApiV01/Contractor/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> GetFields<T>()
        {
            EntityField<T> result = await Network.Get<EntityField<T>>("/BumsCrmApiV01/Contractor/listFields.api");
            return result.Fields;
        }

        public async Task<T> Create<T>(QueryParameters data)
        {
            ContractorResult<T> result = await Network.Post<ContractorResult<T>>("/BumsCrmApiV01/Contractor/save.api", data);
            return result.Result;
        }

        public async Task<T> Edit<T>(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            return await Create<T>(data);
        }

        public async Task Delete(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCrmApiV01/Contractor/delete.api", query);
        }

        public async Task Block(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/UserAccount/block.api", query);
        }

        public async Task Unblock(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/UserAccount/unblock.api", query);
        }
    }
}
