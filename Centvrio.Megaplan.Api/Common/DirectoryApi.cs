﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class DirectoryApi : CommonApi
    {
        public DirectoryApi(ISession session) : base(session)
        {
        }

        public async Task<IList<T>> Currencies<T>(int limit = default, int offset = default)
        {
            CurrencyMultipleResult<T> result = await GetDirectory<CurrencyMultipleResult<T>>("/BumsCommonApiV01/Currency/list.api", limit, offset);
            return result.Result;
        }

        public async Task<IList<T>> PhoneTypes<T>(int limit = default, int offset = default)
        {
            PhoneTypeMultipleResult<T> result = await GetDirectory<PhoneTypeMultipleResult<T>>("/BumsStaffApiV01/Employee/phoneTypes.api", limit, offset);
            return result.Result;
        }

        public async Task<IList<T>> Taxes<T>(int limit = default, int offset = default)
        {
            TaxMultipleResult<T> result = await GetDirectory<TaxMultipleResult<T>>("/BumsCommonApiV01/Tax/list.api", limit, offset);
            return result.Result;
        }

        public async Task<IList<T>> Units<T>(int limit = default, int offset = default)
        {
            UnitMultipleResult<T> result = await GetDirectory<UnitMultipleResult<T>>("/BumsInvoiceApiV01/Unit/list.api", limit, offset);
            return result.Result;
        }

        private async Task<T> GetDirectory<T>(string url, int limit, int offset)
        {
            QueryParameters query = QueryParameters.Create()
                .AddByContidion(() => limit > 0, "Limit", limit)
                .AddByContidion(() => offset > 0, "Offset", offset);
            T result = await Network.Get<T>(url, query);
            return result;
        }
    }
}
