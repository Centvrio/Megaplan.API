﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class EmployeeApi : CommonApi
    {
        public EmployeeApi(ISession session) : base(session)
        {
        }

        public async Task<T> Get<T>(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            EmployeeResult<T> result = await Network.Get<EmployeeResult<T>>("/BumsStaffApiV01/Employee/card.api", query);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            EmployeeMultipleResult<T> result = await Network.Get<EmployeeMultipleResult<T>>("/BumsStaffApiV01/Employee/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAllByOnline<T>()
        {
            EmployeeOnlineResult<T> result = await Network.Get<EmployeeOnlineResult<T>>("/BumsStaffApiV01/Employee/employeesOnline.api");
            return result.Result;
        }

        public async Task<T> Create<T>(QueryParameters data)
        {
            EmployeeResult<T> result = await Network.Post<EmployeeResult<T>>("/BumsStaffApiV01/Employee/create.api", data);
            return result.Result;
        }

        public async Task Edit(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            await Network.Post<EmptyResult>("/BumsStaffApiV01/Employee/edit.api", data);
        }

        public async Task<T> AvailableActions<T>(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            T result = await Network.Get<T, ActionsResponse<T>>("/BumsStaffApiV01/Employee/availableActions.api", query);
            return result;
        }

        public async Task<bool> CanCreate()
        {
            var network = new Network(Session);
            CanActionResult result = await network.Get<CanActionResult>("/BumsStaffApiV01/Employee/canCreate.api");
            return result.CanCreate == true;
        }

        public async Task<IList<T>> GetDepartments<T>()
        {
            EmployeeDepartmentsResult<T> result = await Network.Get<EmployeeDepartmentsResult<T>>("/BumsStaffApiV01/Department/list.api");
            return result.Result;
        }

        public async Task Block(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/UserAccount/block.api", query);
        }

        public async Task Unblock(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/UserAccount/unblock.api", query);
        }

        public async Task Fire(int id)
        {
            var query = QueryParameters.CreateAndAdd("Id", id);
            await Network.Get<EmptyResult>("/BumsCommonApiV01/UserAccount/fire.api", query);
        }
    }
}
