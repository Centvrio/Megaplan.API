﻿using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Extensions;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Common
{
    public class TaskApi : CommonApi
    {
        public TaskApi(ISession session) : base(session)
        {
        }

        public async Task<IList<T>> GetAll<T>(QueryParameters filter)
        {
            TaskMiltipleResult<T> result = await Network.Get<TaskMiltipleResult<T>>("/BumsTaskApiV01/Task/list.api", filter);
            return result.Result;
        }

        public async Task<IList<T>> GetAll<T>()
        {
            return await GetAll<T>(null);
        }

        public async Task<T> Get<T>(int id, QueryParameters filter)
        {
            QueryParameters query = filter ?? QueryParameters.Create();
            query.Add("Id", id);
            TaskResult<T> result = await Network.Get<TaskResult<T>>("/BumsTaskApiV01/Task/card.api", query);
            return result.Result;
        }

        public async Task<T> Get<T>(int id)
        {
            return await Get<T>(id, null);
        }

        public async Task<T> Create<T>(QueryParameters data)
        {
            TaskResult<T> result = await Network.Post<TaskResult<T>>("/BumsTaskApiV01/Task/create.api", data);
            return result.Result;
        }

        public async Task Edit(int id, QueryParameters data)
        {
            QueryParameters preparedData = data ?? QueryParameters.Create();
            preparedData.Add("Id", id);
            await Network.Post<EmptyResult>("/BumsTaskApiV01/Task/edit.api", preparedData);
        }

        public async Task SaveAuditors(int id, params int[] auditors)
        {
            QueryParameters data = QueryParameters.Create()
                .Add("Id", id)
                .AddArray("Auditors", auditors);
            await Network.Post<EmptyResult>("/BumsTaskApiV01/Task/saveAuditors.api", data);
        }

        public async Task ResetAuditors(int id)
        {
            await SaveAuditors(id, null);
        }

        public async Task SaveExecutors(int id, params int[] executors)
        {
            QueryParameters data = QueryParameters.Create()
                .Add("Id", id)
                .AddArray("Executors", executors);
            await Network.Post<EmptyResult>("/BumsTaskApiV01/Task/saveExecutors.api", data);
        }

        public async Task ResetExecutors(int id)
        {
            await SaveExecutors(id, null);
        }

        public async Task Action(int id, string action)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("Action", action);
            await Network.Get<EmptyResult>("/BumsTaskApiV01/Task/action.api", query);
        }

        public async Task Action(int id, TaskActions action)
        {
            await Action(id, action.ToStringAction());
        }

        public async Task<bool> CheckDelegate(int id, int responsibleId)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("ResponsibleId", responsibleId);
            CanActionResult result = await Network.Get<CanActionResult>("/BumsTaskApiV01/Task/checkDelegate.api", query);
            return result.CanDelegate == true;
        }

        public async Task<int> ConvertToProject(int id)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id);
            ProjectResult<EntityId> result = await Network.Get<ProjectResult<EntityId>>("/BumsTaskApiV01/Task/convert.api", query);
            return result.Result;
        }

        public async Task<int> DeadlineChangeRequest(int subjectId, SubjectType subjectType, QueryParameters parameters)
        {
            QueryParameters query = parameters ?? QueryParameters.Create();
            parameters
                .Add("SubjectId", subjectId)
                .Add("SubjectType", subjectType.ToString().ToLower());
            int result = await Network.Get<int, DeadlineChangeResponse>("/BumsTaskApiV01/Task/deadlineChange.api", query);
            return result;
        }

        public async Task ApplyDeadline(int requestId, bool doApply)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", requestId)
                .Add("Action", doApply ? @"act\_accept\_deadline" : @"act\_reject\_deadline");
            await Network.Get<EmptyResult>("/BumsTaskApiV01/Task/deadlineAction.api", query);
        }

        public async Task Delegate(int id, int responsibleId)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("ResponsibleId", responsibleId);
            await Network.Get<EmptyResult>("/BumsTaskApiV01/Task/delegate.api", query);
        }

        public async Task MarkAsFavorite(int id, bool AsFavorite)
        {
            QueryParameters query = QueryParameters.Create()
                .Add("Id", id)
                .Add("Value", AsFavorite ? 1 : 0);
            await Network.Get<EmptyResult>("/BumsTaskApiV01/Task/markAsFavorite.api", query);
        }

        public async Task<T> AvailableActions<T>(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            T result = await Network.Get<T, ActionsResponse<T>>("/BumsTaskApiV01/Task/availableActions.api", query);
            return result;
        }

        public async Task<T> AvailableActionsForList<T>(params int[] ids)
        {
            var query = QueryParameters.Create();
            query.AddArray("Ids", ids);
            T result = await Network.Get<T, ActionsResponse<T>>("/BumsTaskApiV01/Task/availableActionsForList.api", query);
            return result;
        }

        public async Task<IList<T>> GetCanSpecifiedSuperProjects<T>()
        {
            ProjectMultipleResult<T> result = await Network.Get<ProjectMultipleResult<T>>("/BumsTaskApiV01/Task/superProjects.api");
            return result.Result;
        }

        public async Task<IList<T>> GetCanSpecifiedSuperTasks<T>()
        {
            TaskMiltipleResult<T> result = await Network.Get<TaskMiltipleResult<T>>("/BumsTaskApiV01/Task/superTasks.api");
            return result.Result;
        }

        public async Task<IList<T>> GetEmployeesToDelegate<T>(int id)
        {
            QueryParameters query = QueryParameters.Create().Add("Id", id);
            EmployeeMultipleResult<T> result = await Network.Get<EmployeeMultipleResult<T>>("/BumsTaskApiV01/Task/employeesToDelegate.api", query);
            return result.Result;
        }
    }
}
