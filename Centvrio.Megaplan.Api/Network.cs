﻿using Centvrio.Megaplan.Api.Exceptions;
using Centvrio.Megaplan.Api.Extensions;
using Centvrio.Megaplan.Api.Injection;
using Centvrio.Megaplan.Api.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api
{
    public class Network
    {
        private readonly ISession session;

        internal Network(ISession session)
        {
            this.session = session;
        }

        public async Task<T> Get<T, U>(string url) where U : Response<T>
        {
            return await Get<T, U>(url, null);
        }

        public async Task<T> Get<T>(string url)
        {
            return await Get<T, Response<T>>(url, null);
        }

        public async Task<T> Get<T>(string url, QueryParameters queryParams)
        {
            return await Get<T, Response<T>>(url, queryParams);
        }

        public async Task<T> Get<T, U>(string url, QueryParameters queryParams) where U : Response<T>
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                if ((queryParams?.Count ?? 0) > 0)
                {
                    url = $"{url}?{queryParams}";
                }
                Uri uri = url.StartsWith("/") ? new Uri($"https://{session.Host}{url}") : new Uri($"https://{session.Host}/{url}");
                if (!(string.IsNullOrEmpty(session?.AccessId) && string.IsNullOrEmpty(session.SecretKey)))
                {
                    string date = DateTime.Now.ToRfc2822();
                    string sign = CreateSignature("GET", "", date, uri.Host, uri.PathAndQuery);
                    PrepareHeaders(client, sign, date);
                }
                using (HttpResponseMessage response = await client.GetAsync(uri))
                using (HttpContent content = response.Content)
                {
                    return await ResponseHandler<T, U>(response, content);
                }
            }
        }

        public async Task<T> Post<T>(string url, QueryParameters data)
        {
            return await Post<T, Response<T>>(url, data);
        }

        public async Task<T> Post<T, U>(string url, QueryParameters data) where U : Response<T>
        {
            FormUrlEncodedContent formData = new FormUrlEncodedContent(data.Storage) ?? throw new ArgumentNullException(nameof(data));
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                Uri uri = url.StartsWith("/") ? new Uri($"https://{session.Host}{url}") : new Uri($"https://{session.Host}/{url}");
                if (!(string.IsNullOrEmpty(session?.AccessId) && string.IsNullOrEmpty(session.SecretKey)))
                {
                    string date = DateTime.Now.ToRfc2822();
                    string sign = CreateSignature("POST", "application/x-www-form-urlencoded", date, uri.Host, uri.PathAndQuery);
                    PrepareHeaders(client, sign, date);
                }
                using (HttpResponseMessage response = await client.PostAsync(uri, formData))
                using (HttpContent content = response.Content)
                {
                    return await ResponseHandler<T, U>(response, content);
                }
            }
        }

        private void PrepareHeaders(HttpClient client, string rawSignature, string rfcDate)
        {
            byte[] signBytes = Encoding.UTF8.GetBytes(rawSignature.ToHmacSha1(session.SecretKey));
            string signature = Convert.ToBase64String(signBytes);
            client.DefaultRequestHeaders.TryAddWithoutValidation("X-Sdf-Date", rfcDate);
            client.DefaultRequestHeaders.TryAddWithoutValidation("X-Authorization", $"{session.AccessId}:{signature}");
        }

        private async Task<T> ResponseHandler<T, U>(HttpResponseMessage response, HttpContent content) where U : Response<T>
        {
            string json = await content.ReadAsStringAsync();
            var serializer = new JsonSerializer();
            U result = JsonConvert.DeserializeObject<U>(json);
            return result.IsSuccess ? result.Data : throw new CommonApiException(ConvertMessage(result.Status.Message), response.StatusCode);
        }

        private string ConvertMessage(object message)
        {
            switch (message)
            {
                case string s:
                    return s;
                case JArray a:
                    IList<MessageInternal> errors = a.ToObject<IList<MessageInternal>>();
                    var builder = new StringBuilder();
                    foreach (MessageInternal error in errors)
                    {
                        builder.AppendLine($"{error.Field}: {error.Message}");
                    }
                    return builder.ToString();
            }
            return string.Empty;
        }

        private string CreateSignature(string method, string contentType, string date, string host, string uri)
        {
            var builder = new StringBuilder();
            builder
                .Append($"{method}\n")
                .Append("\n")
                .Append($"{contentType}\n")
                .Append($"{date}\n")
                .Append($"{host}{uri}");
            return builder.ToString();
        }
    }
}
