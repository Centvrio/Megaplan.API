﻿using Centvrio.Megaplan.Api.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("System")]
    public class SystemApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetSystemDateTime()
        {
            object result = await core.Take<SystemApi>().DateTime();
            Assert.IsInstanceOfType(result, typeof(DateTimeOffset));
        }

        [TestMethod]
        public async Task Feedback()
        {
            await core.Take<SystemApi>().Feedback("Test message!");
        }
    }
}
