﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Employees")]
    public class EmployeeApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetList()
        {
            object result = await core.Take<EmployeeApi>().GetAll<object>(new QueryParameters
            {
                ["OrderBy"] = "name"
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetListByOnline()
        {
            object result = await core.Take<EmployeeApi>().GetAllByOnline<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<EmployeeApi>().Get<object>(1000004);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            object result = await core.Take<EmployeeApi>().Create<object>(new QueryParameters
            {
                ["Model[Login]"] = "testikk1",
                ["Model[Password]"] = "test1234567",
                ["Model[LastName]"] = "Testoff",
                ["Model[FirstName]"] = "Testikk",
                ["Model[Position]"] = "Продажник",
                ["Model[Email]"] = "testikk1@test.com",
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditEmployee()
        {
            await core.Take<EmployeeApi>().Edit(1000009, new QueryParameters
            {
                ["Model[Position]"] = "Продажник",
            });
        }

        [TestMethod]
        public async Task AvailableActions()
        {
            object result = await core.Take<EmployeeApi>()
                .AvailableActions<object>(1000012);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetDepartments()
        {
            object result = await core.Take<EmployeeApi>().GetDepartments<object>();
            Assert.IsNotNull(result);
        }
    }
}
