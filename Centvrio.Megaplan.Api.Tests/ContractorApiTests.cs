﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Contractors")]
    public class ContractorApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetFields()
        {
            IList<object> result = await core.Take<ContractorApi>().GetFields<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<ContractorApi>().Get<object>(1003805);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCardWithRequestedFields()
        {
            object result = await core.Take<ContractorApi>().Get<object>(1003810, "Id", "FirstName");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetList()
        {
            object result = await core.Take<ContractorApi>().GetAll<object>(new QueryParameters
            {
                ["Limit"] = 10,
                ["Offset"] = 2
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            object result = await core.Take<ContractorApi>().Create<object>(new QueryParameters
            {
                ["Model[FirstName]"] = "Test",
                ["Model[LastName]"] = "Testoff",
                ["Model[TypePerson]"] = "company",
                ["Model[CompanyName]"] = "Test Inc.",
                ["Model[Category183CustomFieldNomerKartiKlienta]"] = "12345678901255",
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditContractor()
        {
            object result = await core.Take<ContractorApi>().Edit<object>(1003815, new QueryParameters
            {
                ["Model[Email]"] = "test_inc@test.com"
            });
            Assert.IsNotNull(result);
        }
    }
}
