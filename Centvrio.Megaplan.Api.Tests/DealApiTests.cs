﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Deals")]
    public class DealApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetFields()
        {
            IList<object> result = await core.Take<DealApi>().GetFields<object>(9);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetProgramSchemas()
        {
            object result = await core.Take<DealApi>().GetProgramSchemas<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            QueryParameters query = QueryParameters.Create()
                .AddArray("RequestedFields", "Name", "Description");
            object result = await core.Take<DealApi>().Get<object>(225, query);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetList()
        {
            IList<object> result = await core.Take<DealApi>().GetAll<object>(new QueryParameters
            {
                ["RequestedFields[]"] = "Name",
                ["RequestedFields[]"] = "Description",
                ["Limit"] = 2
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            object result = await core.Take<DealApi>().Create<object>(new QueryParameters
            {
                ["ProgramId"] = 6,
                ["Model[Description]"] = "Deal Desc",
                ["Model[Contractor]"] = 1003815
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditDeal()
        {
            object result = await core.Take<DealApi>().Edit<object>(267, new QueryParameters
            {
                ["Model[Cost][Value]"] = 1200,
                ["Model[Cost][Currency]"] = 2,
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task SaveRelation()
        {
            object result = await core.Take<DealApi>().SaveRelation<object>(267, 1000196, RelatedObjectType.Task);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task RemoveRelation()
        {
            object result = await core.Take<DealApi>().RemoveRelation<object>(267, 1000196, RelatedObjectType.Task);
            Assert.IsNotNull(result);
        }
    }
}
