﻿using Centvrio.Megaplan.Api.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("ContractorStatus")]
    public class MyTestClass
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task CreateNew()
        {
            int result = await core.Take<ContractorStatusApi>().Create("New Status");
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task EditStatus()
        {
            int result = await core.Take<ContractorStatusApi>().Edit(1000003, "New Contractor");
            Assert.IsNotNull(result > 0);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<ContractorStatusApi>().Get<object>(1000003);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task DeleteStatus()
        {
            await core.Take<ContractorStatusApi>().Delete(1000000);
        }
    }
}
