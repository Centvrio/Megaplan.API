﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Injection.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Object Activator")]
    public class ObjectActivatorTest
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            core = Core.Create(new Session() { Host = "mgtsk.megaplan.ru" });
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public void LastUseTest()
        {
            UserApi firstInstance = core.Take<UserApi>();
            UserApi secondInstance = core.Take<UserApi>();
            Assert.IsTrue(ReferenceEquals(firstInstance, secondInstance));
        }

        [TestMethod]
        public void InstanceCacheTest()
        {
            var activator = new ObjectActivator();
            UserApi objA = activator.CreateInstance<UserApi>(core.Session);
            SystemApi objB = activator.CreateInstance<SystemApi>(core.Session);
            Assert.AreEqual(activator.CacheSize, 2);
        }
    }
}
