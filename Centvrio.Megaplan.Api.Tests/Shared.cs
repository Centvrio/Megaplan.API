﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Injection.Implementation;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    public class Shared
    {
        public async static Task<Core> InitializeTests()
        {
            var core = Core.Create(new Session() { Host = "mgtsk.megaplan.ru" });
            await core.Take<UserApi>().Login("demo@mgtsk.ru", "demo@mgtsk.ru");
            return core;
        }
    }
}
