﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Projects")]
    public class ProjectApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetList()
        {
            object result = await core.Take<ProjectApi>()
                .GetAll<object>(new QueryParameters
                {
                    ["Detailed"] = true,
                    ["SortBy"] = "name",
                    ["SortOrder"] = "desc",
                });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<ProjectApi>().Get<object>(1000003);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNewProject()
        {
            object result = await core.Take<ProjectApi>()
                .Create<object>(new QueryParameters
                {
                    ["Model[Name]"] = "Project from lib",
                    ["Model[Responsible]"] = 1000000,

                });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditProject()
        {
            await core.Take<ProjectApi>()
                .Edit(1000004, QueryParameters.Create().Add("Model[Name]", "Edited project from lib"));
        }

        [TestMethod]
        public async Task AvailableActions()
        {
            object result = await core.Take<ProjectApi>()
                .AvailableActions<object>(1000004);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ConvertToTask()
        {
            int result = await core.Take<ProjectApi>().ConvertToTask(1000015);
            Assert.IsTrue(result > 0);
        }
    }
}
