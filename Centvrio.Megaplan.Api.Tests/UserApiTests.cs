using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Common.Internal;
using Centvrio.Megaplan.Api.Injection.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Authorization")]
    public class UserApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            core = Core.Create(new Session() { Host = "mgtsk.megaplan.ru" });
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetOneTimeKey()
        {
            AuthorizationResult result = await core.Take<UserApi>().GetOneTimeKey("demo@mgtsk.ru", "demo@mgtsk.ru");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Login()
        {
            AuthorizationResult result = await core.Take<UserApi>().Login("demo@mgtsk.ru", "demo@mgtsk.ru");
            Assert.IsNotNull(result);
        }
    }
}
