﻿using Centvrio.Megaplan.Api.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Directories")]
    public class DirectoryApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetCurrency()
        {
            IList<object> result = await core.Take<DirectoryApi>().Currencies<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetPhoneTypes()
        {
            IList<object> result = await core.Take<DirectoryApi>().PhoneTypes<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetTax()
        {
            IList<object> result = await core.Take<DirectoryApi>().Taxes<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetUnit()
        {
            IList<object> result = await core.Take<DirectoryApi>().Units<object>();
            Assert.IsNotNull(result);
        }
    }
}
