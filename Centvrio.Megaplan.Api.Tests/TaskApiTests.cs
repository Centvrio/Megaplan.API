﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Tasks")]
    public class TaskApiTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetList()
        {
            object result = await core.Take<TaskApi>().GetAll<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetListWithFilter()
        {
            object result = await core.Take<TaskApi>().GetAll<object>(new QueryParameters
            {
                ["Folder"] = "incoming",
                ["SortOrder"] = "asc"
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<TaskApi>().Get<object>(1000183);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCardWithFilter()
        {
            object result = await core.Take<TaskApi>().Get<object>(1000183, new QueryParameters
            {
                ["RequestedFields[]"] = "Id",
                ["RequestedFields[]"] = "Name"
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNewTask()
        {
            object result = await core.Take<TaskApi>().Create<object>(new QueryParameters
            {
                ["Model[Name]"] = "Task from lib 3",
                ["Model[Responsible]"] = 1000000,
                ["Model[Deadline]"] = DateTime.UtcNow.AddDays(1)
            });
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditTask()
        {
            await core.Take<TaskApi>().Edit(1000189, QueryParameters.Create().Add("Model[Name]", "Edit from lib"));
        }

        [TestMethod]
        public async Task SaveAuditors()
        {
            await core.Take<TaskApi>().SaveAuditors(1000189, 1000003, 1000006);
        }

        [TestMethod]
        public async Task SaveExecutors()
        {
            await core.Take<TaskApi>().SaveExecutors(1000189, 1000004, 1000005);
        }

        [TestMethod]
        public async Task ConvertToProject()
        {
            int result = await core.Take<TaskApi>().ConvertToProject(1000204);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task DeadlineChangeRequest()
        {
            int result = await core.Take<TaskApi>()
                .DeadlineChangeRequest(1000195, SubjectType.Task, QueryParameters.Create()
                .Add("Deadline", DateTime.UtcNow.AddDays(3))
                .Add("Request", "Test request comment"));
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task AvailableActions()
        {
            string[] result = await core.Take<TaskApi>().AvailableActions<string[]>(1000191);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCanSpecifiedSuperProjects()
        {
            object result = await core.Take<TaskApi>().GetCanSpecifiedSuperProjects<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCanSpecifiedSuperTasks()
        {
            object result = await core.Take<TaskApi>().GetCanSpecifiedSuperTasks<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetEmployeesToDelegate()
        {
            object result = await core.Take<TaskApi>().GetEmployeesToDelegate<object>(1000191);
            Assert.IsNotNull(result);
        }
    }
}
