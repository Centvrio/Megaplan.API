﻿using Centvrio.Megaplan.Api.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("TodoList")]
    public class TodoListTests
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetList()
        {
            object result = await core.Take<TodoListApi>().GetAll<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            int result = await core.Take<TodoListApi>().Create("Test todo 1");
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task Edit()
        {
            int result = await core.Take<TodoListApi>().Edit(24, "Test todo edited");
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task Delete()
        {
            int result = await core.Take<TodoListApi>().Delete(20);
            Assert.IsTrue(result > 0);
        }

    }
}
