﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Events")]
    public class EventApiTest
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetList()
        {
            IList<object> result = await core.Take<EventApi>().GetAll<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetListWithFilter()
        {
            var filter = QueryParameters.CreateAndAdd("OnlyActual", true);
            IList<object> result = await core.Take<EventApi>().GetAll<object>(filter);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<EventApi>().Get<object>(735);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCategories()
        {
            var filter = QueryParameters.CreateAndAdd("Name", "Спорт");
            IList<object> result = await core.Take<EventApi>().Categories<object>(filter);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetPlaces()
        {
            IList<object> result = await core.Take<EventApi>().Places<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            QueryParameters data = QueryParameters
                .CreateAndAdd("Model[Name]", "Test Event")
                .Add("Model[Description]", "Test Event Description");
            IDictionary<string, object> result = await core.Take<EventApi>().Create<object>(data);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EditEvent()
        {
            QueryParameters data = QueryParameters
                .CreateAndAdd("Model[Name]", "Test Event Edited")
                .Add("Model[Description]", "Test Event Description Edited");
            IDictionary<string, object> result = await core.Take<EventApi>().Edit<object>(844, data);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task FinistTest()
        {
            await core.Take<EventApi>().Finish(844, true, "Test Finish Result");
        }

        [TestMethod]
        public async Task DeleteEvent()
        {
            await core.Take<EventApi>().Delete(844);
        }
    }
}
