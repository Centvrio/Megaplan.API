﻿using Centvrio.Megaplan.Api.Common;
using Centvrio.Megaplan.Api.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Payers")]
    public class PayerApiTest
    {
        private static Core core;

        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public async static Task ClassInitialize(TestContext context)
        {
            core = await Shared.InitializeTests();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [TestMethod]
        public async Task GetCard()
        {
            object result = await core.Take<PayerApi>().Get<object>(1003824);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetList()
        {
            IList<object> result = await core.Take<PayerApi>().GetAll<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetListWithFilter()
        {
            var filter = QueryParameters.CreateAndAdd("FilterFields[Contractor]", 1000377);
            IList<object> result = await core.Take<PayerApi>().GetAll<object>();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetPayerFields()
        {
            object result = await core.Take<PayerApi>().GetFields<object>(PayerType.Legal, "UA");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CreateNew()
        {
            int result = await core.Take<PayerApi>().Create(new QueryParameters
            {
                ["ContractorId"] = 1000000,
                ["PayerType"] = "Natural",
                ["Model[FirstName]"] = "Test Payer Name",
                ["Model[LastName]"] = "Test Payer Second Name"
            });
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public async Task EditPayer()
        {
            int expected = 1003824;
            int actual = await core.Take<PayerApi>().Edit(1003824, new QueryParameters
            {
                ["Model[FirstName]"] = "Test Payer Name",
                ["Model[LastName]"] = "Test Payer Second Name"
            });
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task ChangeContractor()
        {
            await core.Take<PayerApi>().ChangeContractor(1003824, 1000000, 1000377);
        }
    }
}
