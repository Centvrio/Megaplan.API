﻿using Centvrio.Megaplan.Api.Exceptions;
using Centvrio.Megaplan.Api.Injection.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Centvrio.Megaplan.Api.Tests
{
    [TestClass]
    [TestCategory("Api Core")]
    public class ApiCoreTests
    {
        public TestContext TestContext { get; set; }

        [TestCleanup]
        public void TestCleanup()
        {
            TestContext.WriteLine($"{TestContext.TestName}: {TestContext.CurrentTestOutcome}");
        }

        [ExpectedException(typeof(CoreException))]
        [TestMethod]
        public void CreateWithCoreException()
        {
            Core.Create(new Session());
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void CreateWithArgumentNullException()
        {
            Core.Create<Session>(null);
        }

        [TestMethod]
        public void IsCoreCreated()
        {
            var core = Core.Create(new Session() { Host = "mgtsk.megaplan.ru" });
            bool expected = true;
            bool actual = core.IsCreated;
            Assert.AreEqual(expected, actual);
        }
    }
}
